package com.icebullet.certificateChainList

import android.app.KeyguardManager
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.security.keystore.KeyInfo
import android.security.keystore.UserNotAuthenticatedException
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.PopupMenu
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.DividerItemDecoration
import com.icebullet.certificateChainList.keystore.Keystore
import com.icebullet.certificateChainList.net.AppActivityConnector
import com.icebullet.certificateChainList.net.NetRequest
import com.icebullet.certificateChainList.utils.SupervisorScope
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withTimeout
import java.nio.charset.Charset
import java.security.KeyStore
import java.security.cert.Certificate
import java.security.cert.X509Certificate
import javax.crypto.SecretKey
import javax.crypto.SecretKeyFactory

class MainActivity : AppCompatActivity() {

    companion object {
        const val RSA_ALIAS = "rsa_alias"

        const val ALIAS = "sample_alias"
        const val ALIAS_2 = "sample_alias_2"
        const val ANDROID_KEYSTORE = "AndroidKeyStore"
        const val TAG = "KeyStoreResearch"
        const val SHA256withECDSA = "SHA256withECDSA"

        const val TRANSFORMATION = "AES/CBC/PKCS7Padding"

        const val sampleStringData = "Hello, world!"

        @JvmStatic
        @BindingAdapter("bindCertBefore")
        fun bindCertBefore(textView: TextView, certificate: X509Certificate) {
            textView.text = certificate.notBefore.toString()
        }

        @JvmStatic
        @BindingAdapter("bindCertAfter")
        fun bindCertAfter(textView: TextView, certificate: X509Certificate) {
            textView.text = certificate.notAfter.toString()
        }

        @JvmStatic
        @BindingAdapter("bindCertAction")
        fun bindCertAction(view: View, certificate: X509Certificate) {
            view.setOnClickListener {
                PopupMenu(view.context, view).also {
                    it.menu.run {
                        add("Share")
                    }
                    it.setOnMenuItemClickListener {
                        shareText(view.context, certificate.toPemString())

                        true
                    }
                }.show()
            }
        }

        private fun shareText(context: Context, text: String) {
            val intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, text)
                type = "text/plain"
            }
            context.startActivity(Intent.createChooser(intent, "Select app"))
        }

        private fun Certificate.toPemString(): String {
            val base64String = Base64.encode(encoded, Base64.DEFAULT)
                .toString(Charset.defaultCharset())
            return "-----BEGIN CERTIFICATE-----\n$base64String-----END CERTIFICATE-----"
        }
    }

    val keystore: Keystore by lazy {
        Keystore()
    }

    private val adapter = com.icebullet.certificateChainList.view.Adapter<X509Certificate>(
        R.layout.item_certificate,
        BR.item
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AppActivityConnector.onActivityStart(this)

        setContentView(R.layout.activity_main)
        recycler.adapter = adapter
        recycler.addItemDecoration(
            DividerItemDecoration(
                this,
                DividerItemDecoration.VERTICAL
            )
        )

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            //todo
            generateAndPrintCertV2(RSA_ALIAS)
        } else {
            err_view.text =
                "Api level ${Build.VERSION_CODES.N} is required.\nYour device version: ${Build.VERSION.SDK_INT}"
            fab.hide()
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        AppActivityConnector.onActivityStop(this)
    }

    fun onShareClick(view: View) {
        val pemString = adapter.items.map {
            it.toPemString()
        }.joinToString("\n\n")

        shareText(this, pemString)
    }

    fun onSendClick(view: View) {
        try {

            //keystore.encryptDecrypt("hellow", ALIAS)
        } catch (ex: UserNotAuthenticatedException) {
            showAuthenticationScreen()
            return
        }

        //showAuthenticationScreen()

        SupervisorScope.async(Dispatchers.Main) {
            if (showAuthenticationScreen()) {

                SupervisorScope.async(Dispatchers.IO) {
                    withTimeout(120000) {
                        //NetRequest.NetRequestImp.callmeCheck(Unit)
                        try {

                            NetRequest.CKImp.ckTest()
                        } catch (ex: UserNotAuthenticatedException) {
                            showAuthenticationScreen()
                        }
                    }
                }
            }

        }

    }

    private final val REQUEST_CODE_CREDENTIALS = 1000


    var init = false
    private fun showAuthenticationScreen(): Boolean {
        //baseContext
        //  val keyguardManager = baseContext.getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
        val keyguardManager = getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
        val hard = keyguardManager.isKeyguardSecure

        if (hard && init) {
            return true
        }

        init = true

        // BiometricPrompt.Builder().build().

        val intent = keyguardManager.createConfirmDeviceCredentialIntent("Hey there!", "Please...!!!!!");

        if (intent != null) {
            startActivityForResult(intent, REQUEST_CODE_CREDENTIALS);
        }

        return false
    }

    private fun checkHardware(alias: String): String {
        return if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            "Hardware keystore is not supported on this api level (${Build.VERSION.SDK_INT}): required api ${Build.VERSION_CODES.M}"
        } else {
            try {
                val key = keystore.getOrCreateSecretKey(alias)
                val isHardware = key.isInsideSecureHardware()
                "Hardware keystore: $isHardware"
            } catch (err: Throwable) {
                "Cannot get information about keystore, error: ${err.message}"
            }
        }
    }

    fun SecretKey.isInsideSecureHardware(): Boolean {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) return false
        val scf = SecretKeyFactory.getInstance(
            algorithm,
            ANDROID_KEYSTORE
        )
        return try {
            val keyInfo = scf.getKeySpec(this, KeyInfo::class.java) as KeyInfo
            keyInfo.isInsideSecureHardware
        } catch (err: Throwable) {
            err.printStackTrace()
            false
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun generateAndPrintCertV2(alias: String) {
        keystore.generateAndPrintCertV2(alias)

        val certs = keystore.ks.getCertificateChain(alias)
        adapter.items = certs.toList().map { it as X509Certificate }

        if (adapter.itemCount == 0) {
            err_view.text = "Цепочка сертификатов пуста"
            err_view.visibility = View.VISIBLE
            fab.hide()
        }

        val keyHardwarePropertiesManager = keystore.isInsideSecurityHardware(alias)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun generateAndPrintCert(alias: String) {
        if (!keystore.ks.containsAlias(alias)) {
            keystore.generateKeyPair(alias)
            Log.d(TAG, "generated new key pair")
        }

        val entry = keystore.ks.getEntry(alias, null)
        if (entry is KeyStore.PrivateKeyEntry) {
            val cert = entry.certificate
            Log.d(TAG, cert.toString())
            Log.d(TAG, "PEM:\n" + cert.toPemString())
        }
    }

//    fun onButtonClick(@Suppress("UNUSED_PARAMETER") button : View) {
//        try {
//            val alias = "rsa_alias"
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                generateAndPrintCertV2(alias)
//            }
//
////            ks.aliases().asSequence().forEach {
////                Log.d(TAG,"entry: "+it)
////            }
//
////            val cert = readCertFromFile()
////            bar()
////            readCertFromFile()
////            generateAndPrintCert()
////            baz()
////            val key = getOrCreateSecretKey()
////            Log.d(TAG,"isSecureHardware: ${key.isInsideSecureHardware()}")
//        }catch (err : Throwable) {
//            err.printStackTrace()
//        }
//    }


}
