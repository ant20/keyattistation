package com.icebullet.certificateChainList.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

class Adapter<T>(private val layoutId: Int, private val brId: Int) :
    RecyclerView.Adapter<Adapter.VH>() {
    var items: List<T> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): VH {
        val vdb = DataBindingUtil.inflate<ViewDataBinding>(
            LayoutInflater.from(p0.context),
            layoutId,
            p0,
            false
        )
        return VH(vdb)
    }

    override fun onBindViewHolder(p0: VH, p1: Int) {
        p0.vdb.setVariable(brId, items[p1])
    }

    class VH(val vdb: ViewDataBinding) : RecyclerView.ViewHolder(vdb.root)
}
