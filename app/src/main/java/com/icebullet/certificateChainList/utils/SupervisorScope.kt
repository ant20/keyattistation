package com.icebullet.certificateChainList.utils

import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

object SupervisorScope : CoroutineScope {
    val job = SupervisorJob()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

}

@Suppress("FunctionName")
fun IO(block: suspend CoroutineScope.() -> Unit) {
    SupervisorScope.launch(Dispatchers.IO, block = block)
}