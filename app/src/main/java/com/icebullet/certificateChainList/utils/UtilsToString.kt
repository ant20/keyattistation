package com.icebullet.certificateChainList.utils

import android.content.res.Resources
import android.util.Base64
import android.util.Log
import com.icebullet.certificateChainList.MainActivity
import com.icebullet.certificateChainList.R
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate

private fun readCertFromFile(resources: Resources): X509Certificate {
    val pemString = resources.openRawResource(R.raw.cert)
        .reader()
        .readLines()
        .asSequence()
        .filterNot { it.contains("-----BEGIN CERTIFICATE-----") || it.contains("-----END CERTIFICATE-----") }
        .joinToString(
            separator = ""
        )

    val decoded = Base64.decode(
        pemString.toByteArray(),
        0
    ) /*Base64.encodeBase64(pemString.toByteArray())*/
    val cf = CertificateFactory.getInstance("X.509")
    val cert = cf.generateCertificate(decoded.inputStream()) as X509Certificate
    Log.d(MainActivity.TAG, "cert: $cert")
    return cert
}

private fun X509Certificate.toShortString(): String {
    return StringBuilder().also {
        //            it.append(this.sigAlgName).append('\n')
        it.append("Signature Algorithm:\t ").append(sigAlgName).newLine()
        it.append("Issuer:\t ").append(issuerDN).newLine()
        it.append("Validity: ").newLine()
            .append("\tNot before: ").append(notBefore).newLine()
            .append("\tNot after: ").append(notAfter).newLine()
//            it.append("Validity: ").append(safeCheckValidity()).newLine()
    }.toString()
}

private fun StringBuilder.newLine(): StringBuilder {
    return append("\n")
}
