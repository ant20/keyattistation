package com.icebullet.certificateChainList.keystore

import android.os.Build
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyInfo
import android.security.keystore.KeyProperties
import android.util.Log
import androidx.annotation.RequiresApi
import com.icebullet.certificateChainList.MainActivity
import com.icebullet.certificateChainList.MainActivity.Companion.TRANSFORMATION
import java.math.BigInteger
import java.nio.charset.Charset
import java.security.*
import java.security.cert.X509Certificate
import java.util.*
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey
import javax.crypto.spec.IvParameterSpec
import javax.security.auth.x500.X500Principal


class Keystore {

    val ks by lazy {
        KeyStore.getInstance(MainActivity.ANDROID_KEYSTORE).apply {
            load(null)
        }
    }

    lateinit var iv: ByteArray

    private fun checkCertValidity() {
        val chain = ks.getCertificateChain(MainActivity.ALIAS)
        for (cert in chain) {
            if (cert is X509Certificate) {
                val isValid = try {
                    cert.checkValidity(Date(System.currentTimeMillis()))
                    true
                } catch (err: Throwable) {
                    false
                }
                Log.d(MainActivity.TAG, "cert validity: $isValid")
            } else {
                Log.d(MainActivity.TAG, "cert is not an X509")
            }
        }
    }

    fun X509Certificate.safeCheckValidity(): Boolean {
        return try {
            checkValidity(Date(System.currentTimeMillis()))
            true
        } catch (err: Throwable) {
            false
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun getOrCreateSecretKey(alias: String): SecretKey {
        return if (ks.containsAlias(alias)) {
            val entry = ks.getEntry(alias, null)

            val keystore = entry as KeyStore.SecretKeyEntry
            keystore.secretKey
        } else {
            val keyGenerator = KeyGenerator.getInstance(
                KeyProperties.KEY_ALGORITHM_AES,
                MainActivity.ANDROID_KEYSTORE
            )
            val spec = KeyGenParameterSpec.Builder(alias, KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT)
                .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                .setUserAuthenticationRequired(true)
                .setUserAuthenticationValidityDurationSeconds(500)
                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                .build()

            keyGenerator.init(spec)
            keyGenerator.generateKey()
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun generateAndPrintCertV2(alias: String) {
        if (ks.containsAlias(alias)) {
            //We create the start and expiry date for the key
            val startDate = Calendar.getInstance()
            val endDate = Calendar.getInstance()
            endDate.add(Calendar.YEAR, 1)

            //We are creating the key pair with sign and verify purposes
            val parameterSpec: KeyGenParameterSpec = KeyGenParameterSpec.Builder(
                alias,
                KeyProperties.PURPOSE_SIGN or
                        KeyProperties.PURPOSE_VERIFY //or
                //28 api
                //KeyProperties.PURPOSE_WRAP_KEY or
                //KeyProperties.PURPOSE_ENCRYPT or
                //KeyProperties.PURPOSE_DECRYPT
            )
                .run {
                    setCertificateSerialNumber(BigInteger.valueOf(777))
                    setCertificateSubject(X500Principal("CN=$alias my my my my"))
                    setDigests(
                        KeyProperties.DIGEST_NONE,
                        KeyProperties.DIGEST_SHA256,
                        //KeyProperties.DIGEST_SHA384,
                        KeyProperties.DIGEST_SHA512
                    )
                    setSignaturePaddings(KeyProperties.SIGNATURE_PADDING_RSA_PKCS1)
                    setCertificateNotBefore(startDate.time)
                    setCertificateNotAfter(endDate.time)
                    setKeyValidityStart(startDate.time)
                    setKeyValidityEnd(endDate.time)
                    setUserAuthenticationRequired(true)
                    setUserAuthenticationValidityDurationSeconds(5000)
                    setAttestationChallenge("hello world".toByteArray())
                    build()
                }

            val kpg = KeyPairGenerator.getInstance(
                KeyProperties.KEY_ALGORITHM_RSA, MainActivity.ANDROID_KEYSTORE
            ).apply {
                initialize(parameterSpec)
            }

            val keyPair = kpg.generateKeyPair()
            //  val privateKey = keyPair.private

            // val factory = KeyFactory.getInstance(privateKey.getAlgorithm(), "AndroidKeyStore")
            //val keyInfo = factory.getKeySpec(privateKey, KeyInfo::class.java)
            // val secure = keyInfo.isInsideSecureHardware


            // isInsideSecurityHardware(alias)
        }
    }

    /**
     * Generate a new EC key pair entry in the Android Keystore by
     * using the KeyPairGenerator API. The private key can only be
     * used for signing or verification and only with SHA-256 or
     * SHA-512 as the message digest.
     */
    @RequiresApi(Build.VERSION_CODES.M)
    fun generateKeyPair(alias: String): KeyPair {
        val kpg: KeyPairGenerator = KeyPairGenerator.getInstance(
            KeyProperties.KEY_ALGORITHM_EC,
            MainActivity.ANDROID_KEYSTORE
        )
        val parameterSpec: KeyGenParameterSpec = KeyGenParameterSpec.Builder(
            alias,
            KeyProperties.PURPOSE_SIGN or KeyProperties.PURPOSE_VERIFY
        ).run {
            setDigests(KeyProperties.DIGEST_SHA256, KeyProperties.DIGEST_SHA512)
            build()
        }

        kpg.initialize(parameterSpec)

        return kpg.generateKeyPair()
    }

    /**
     * Use a PrivateKey in the KeyStore to create a signature over
     * some data.
     */
    fun getSignature(privateKeyEntryAlias: String, data: ByteArray): ByteArray? {
        val entry: KeyStore.Entry = ks.getEntry(privateKeyEntryAlias, null)
        if (entry !is KeyStore.PrivateKeyEntry) {
            Log.w(MainActivity.TAG, "Not an instance of a PrivateKeyEntry")
            return null
        }
        return Signature.getInstance(MainActivity.SHA256withECDSA).run {
            initSign(entry.privateKey)
            update(data)
            sign()
        }
    }

    /**
     * Verify a signature previously made by a PrivateKey in our
     * KeyStore. This uses the X.509 certificate attached to our
     * private key in the KeyStore to validate a previously
     * generated signature.
     */
    fun verifySignature(
        privateKeyEntryAlias: String,
        data: ByteArray,
        signature: ByteArray
    ): Boolean {
        val entry = ks.getEntry(privateKeyEntryAlias, null) as? KeyStore.PrivateKeyEntry
        if (entry == null) {
            Log.w(MainActivity.TAG, "Not an instance of a PrivateKeyEntry")
            return false
        }
        return Signature.getInstance(MainActivity.SHA256withECDSA).run {
            initVerify(entry.certificate)
            update(data)
            verify(signature)
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun isInsideSecurityHardware(alias: String) {
        val key = ks.getKey(alias, null)

        Log.d(MainActivity.TAG, "key: $key")

        try {

            val privateKey = key as PrivateKey
            val keyFactory = KeyFactory.getInstance(privateKey.algorithm, "AndroidKeyStore")
            val keyInfo = keyFactory.getKeySpec(privateKey, KeyInfo::class.java)
            val res = keyInfo.isInsideSecureHardware
        } catch (e: Exception) {

        }


        /*if (key is SecretKey) {
        val scf = SecretKeyFactory.getInstance(key.algorithm, MainActivity.ANDROID_KEYSTORE)
        val keyInfo = scf.getKeySpec(key, KeyInfo::class.java) as KeyInfo
        Log.d(MainActivity.TAG, "isInsideSecurityHardware(): ${keyInfo.isInsideSecureHardware}")
    } else {
        Log.d(MainActivity.TAG, "not a secret key")
    }*/

    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun encrypt(data: ByteArray, alias: String): ByteArray {
        val secretKey = getOrCreateSecretKey(alias)

        val cipher = Cipher.getInstance(TRANSFORMATION)
        cipher.init(Cipher.ENCRYPT_MODE, secretKey)

        iv = cipher.iv

        return cipher.doFinal(data)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun decrypt(encrypted: ByteArray, alias: String): ByteArray {
        return decrypt(encrypted, getOrCreateSecretKey(alias))
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun decrypt(encrypted: ByteArray, secretKey: SecretKey): ByteArray {
        val cipher = Cipher.getInstance(TRANSFORMATION)
        //val spec = GCMParameterSpec(128, iv)
        //cipher.init(Cipher.DECRYPT_MODE, secretKey, spec)
        cipher.init(Cipher.DECRYPT_MODE, secretKey, IvParameterSpec(iv))

        return cipher.doFinal(encrypted)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun encryptDecrypt(data: String, alias: String) {
        //Log.d(TAG, "data for encrypt: $sampleStringData")

        val secretKey = getOrCreateSecretKey(alias)

        val dataBytes = data.toByteArray()
        val encrypted = encrypt(dataBytes, alias)
        //Log.d(TAG, "lenth: ${encrypted.size} encrypted: $encrypted")
        val decrypted = decrypt(encrypted, secretKey)

        val decryptedString = decrypted.toString(Charset.defaultCharset())
        Log.d("encryptDecrypt", "data after encrypt/decrypt: $decryptedString")
    }
}