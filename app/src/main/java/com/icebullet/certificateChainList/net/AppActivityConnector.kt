package com.icebullet.certificateChainList.net

import com.icebullet.certificateChainList.MainActivity
import java.util.concurrent.CopyOnWriteArraySet

/**
 * Доступ к текущему экземпляру MainActivity
 */
object AppActivityConnector {

    var activity: MainActivity? = null
        private set(value) {
            field = value
            onActivityChangedListeners.forEach {
                it.invoke(value)
            }
        }

    private val onActivityChangedListeners = CopyOnWriteArraySet<(MainActivity?) -> Unit>()

    fun onActivityStart(activity: MainActivity) {
        synchronized(this) {
            if (activity != this.activity) {
                this.activity = activity
            }
        }
    }

    fun onActivityStop(activity: MainActivity) {
        synchronized(this) {
            if (activity == this.activity) {
                this.activity = null
            }
        }
    }
}