package com.icebullet.certificateChainList.net

import okhttp3.Cookie
import okhttp3.CookieJar
import okhttp3.HttpUrl
import okhttp3.OkHttpClient

private fun OkHttpClient.Builder.cookieJar(block: (url: HttpUrl) -> MutableList<Cookie>): OkHttpClient.Builder {
    return this.cookieJar(object : CookieJar {
        override fun saveFromResponse(url: HttpUrl, cookies: MutableList<Cookie>) {
        }

        override fun loadForRequest(url: HttpUrl): MutableList<Cookie> {
            return block(url)
        }
    })
}

fun OkHttpClient.Builder.addCookieJar() = cookieJar {
    mutableListOf(
        Cookie.Builder()
            .domain(it.host())
            .name("SESSION_TOKEN")
            .value("SESSION_TOKEN")
            .build()
    )
}