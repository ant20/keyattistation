package com.icebullet.certificateChainList.net

import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface INetRequestImp {
    @Timeout(40)
    @POST("ver2/callme/check")
    suspend fun callmeCheck(@Body unit: Unit)

    @Timeout(40)
    @GET("search/?text=&lr=50")
    suspend fun bla()

    @Timeout(40)
    @GET("verify_cert")
    suspend fun ckTest()
}