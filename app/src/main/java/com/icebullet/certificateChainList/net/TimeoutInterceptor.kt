package com.icebullet.certificateChainList.net

import okhttp3.Interceptor
import okhttp3.Response
import retrofit2.Invocation
import java.util.concurrent.TimeUnit


@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class Timeout(
    val value: Int = 60,
    val writeTimeout: Int = -1,
    val readTimeout: Int = -1,
    val connectTimeout: Int = -1,
    val timeUnit: TimeUnit = TimeUnit.SECONDS
)

object TimeoutInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val timeout = try {
            request.tag(Invocation::class.java)
                ?.method()
                ?.getAnnotation(Timeout::class.java)
        } catch (err: Throwable) {
            null
        }
        return if (timeout != null) {
            val defTimeout = timeout.value
            chain.withWriteTimeout(timeout.writeTimeout.takeIf { it != -1 } ?: defTimeout, timeout.timeUnit)
                .withReadTimeout(timeout.readTimeout.takeIf { it != -1 } ?: defTimeout, timeout.timeUnit)
                .withConnectTimeout(timeout.connectTimeout.takeIf { it != -1 } ?: defTimeout, timeout.timeUnit)
                .proceed(request)

        } else {
            chain.proceed(request)
        }
    }
}