package com.icebullet.certificateChainList.net

import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class WrappedException internal constructor(cause: String) : IOException(cause)

object NetworkExceptionInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        return chain.proceed(chain.request()).also {

            if (it.code() != 200) {
                val body = it.body()?.string()

                if (body != null) {
                    throw WrappedException(body)
                }
            }
        }
    }
}