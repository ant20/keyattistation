package com.icebullet.certificateChainList.net

import android.util.Log
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.HttpURLConnection

object NetRequest {

    private inline fun HttpURLConnection.use(block: HttpURLConnection.() -> Unit) {
        var connected = false
        try {
            connect()
            connected = true
            block.invoke(this)
        } finally {
            if (connected) {
                try {
                    disconnect()
                } catch (ignored: Throwable) {
                }
            }
        }
    }

    object NetRequestImp : INetRequestImp by createNetWork("https://mapi.ckassa.ru/", "mapi.ckassa.ru")
    object YAImp : INetRequestImp by createNetWork("https://ya.ru", "ya.ru")

    object CKImp : INetRequestImp by createNetWork("https://dev.autopays.ru", "dev.autopays.ru")

    private fun createNetWork(baseUrl: String, host: String): INetRequestImp {
        val certificateServer = HttpCryptoUtils.certificateServer

        try {
            val httpsClient = OkHttpClient.Builder()
                .followRedirects(false)
                .addInterceptor(NetworkExceptionInterceptor)
                .addInterceptor(TimeoutInterceptor)
                /*.applyIf(UrlsManager.isTestServer){
                addInterceptor {
                    val request = it.request().newBuilder()
                        .addHeader("dn", "/C=RU/ST=Moscow/O=ProcessingAgency/OU=Business/CN=PA-ANDROID-Autopays_Android_v13")
                        .addHeader("X-Real-IP", "0.0.0.0")
                        .build()
                    it.proceed(request)
                }
            }*/
                .apply {
                    val pair = HttpCryptoUtils.createSslSocketFactoryAndTrustManager(certificateServer)
                    sslSocketFactory(pair.first, pair.second)
                }
                .apply {
                    hostnameVerifier(VerifierImpl(host, certificateServer))
                }
                .addCookieJar()
                .addInterceptor(HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                    //level = HttpLoggingInterceptor.Level.BASIC
                }).build()

            return Retrofit.Builder()
                .client(httpsClient)
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().setLenient().create()))
                .baseUrl(baseUrl).build().create(INetRequestImp::class.java)
        } catch (ex: Throwable) {
            Log.d("createNetWork", "builder", ex)
            throw ex
        }
    }
}