package com.icebullet.certificateChainList.net

import android.util.Log
import java.security.cert.X509Certificate
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLSession

class VerifierImpl(val expectedHost: String, val trustCertificate: X509Certificate) : HostnameVerifier {

    override fun verify(hostname: String?, session: SSLSession?): Boolean {
        Log.d("VERIFIER", "expected host: $expectedHost hostname: $hostname")
        if (expectedHost != hostname || session == null) return false

        val x509 = session.peerCertificates[0] as X509Certificate

        try {
            x509.checkValidity()
        } catch (err: Throwable) {
            err.printStackTrace()
            return false
        }


        return true

        /*if (trustCertificate.publicKey != x509.publicKey) return false
        if (trustCertificate != x509) return false

        val trustSubjectName = trustCertificate.subjectDN.name
        val x509SubjectName = x509.subjectDN.name

        return !(x509SubjectName.isNullOrBlank() || trustSubjectName.isNullOrBlank() || trustSubjectName != x509SubjectName)*/
    }
}