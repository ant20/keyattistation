package com.icebullet.certificateChainList.net

import java.security.KeyStore
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate
import javax.crypto.Cipher
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec
import javax.crypto.spec.PBEParameterSpec
import javax.net.ssl.KeyManagerFactory
import javax.net.ssl.SSLSocketFactory
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager


object HttpCryptoUtils {

    private val PASSWORD = "enfldsgbnlsngdlksdsgm".toCharArray()
    private val SALT =
        byteArrayOf(0xde.toByte(), 0x33.toByte(), 0x10.toByte(), 0x12.toByte(), 0xde.toByte(), 0x33.toByte(), 0x10.toByte(), 0x12.toByte())

    private val clientPassword: CharArray by lazy {
        //return@lazy PASSWORD
        val keyFactory = SecretKeyFactory.getInstance("PBEWithMD5AndDES")
        val key = keyFactory.generateSecret(PBEKeySpec(PASSWORD))
        val pbeCipher = Cipher.getInstance("PBEWithMD5AndDES")
        pbeCipher.init(Cipher.DECRYPT_MODE, key, PBEParameterSpec(SALT, 20))

        val encryptedPassword = byteArrayOf(
            0x91.toByte(),
            0xb1.toByte(),
            0xf4.toByte(),
            0x07.toByte(),
            0xe2.toByte(),
            0xee.toByte(),
            0x58.toByte(),
            0x0e.toByte(),
            0xda.toByte(),
            0xe2.toByte(),
            0x7d.toByte(),
            0x1d.toByte(),
            0xca.toByte(),
            0x3e.toByte(),
            0x4a.toByte(),
            0x65.toByte()
        )

        String(pbeCipher.doFinal(encryptedPassword), Charsets.UTF_8).toCharArray()
    }

    private const val CLIENT_CERT = com.icebullet.certificateChainList.R.raw.certificate_client
    private const val SERVER_CERT = com.icebullet.certificateChainList.R.raw.certificate_server

    val certificateServer: X509Certificate by lazy {
        AppActivityConnector.activity!!.resources.openRawResource(SERVER_CERT).use {
            CertificateFactory.getInstance("X.509").generateCertificate(it) as X509Certificate
        }
    }

    @Throws(Exception::class)
    fun createSslSocketFactoryAndTrustManager(sert: X509Certificate): Pair<SSLSocketFactory, X509TrustManager> {
        val trustStore = KeyStore.getInstance(KeyStore.getDefaultType())
        trustStore.load(null, null)
        trustStore.setCertificateEntry("server_sert", sert)

        val trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
        trustManagerFactory.init(null as KeyStore?)

        //val keyStore = KeyStore.getInstance("PKCS12")
        // AppActivityConnector.activity!!.resources.openRawResource(CLIENT_CERT).use {
        //     keyStore.load(it, clientPassword)
        //}

        val hardKeystore = AppActivityConnector.activity!!.keystore.ks
        // KeyStore.getInstance(MainActivity.ANDROID_KEYSTORE).apply {
        //            load(null)
        //        }

        //val secretKey = hardKeystore.getKey(MainActivity.RSA_ALIAS, null) as PrivateKey
        val keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm())
        //keyManagerFactory.init(keyStore, clientPassword)
        keyManagerFactory.init(hardKeystore, null)

        val trustManager: X509TrustManager
        if (trustManagerFactory.trustManagers.size != 1 || trustManagerFactory.trustManagers[0] !is X509TrustManager) {
            throw IllegalStateException("Unknown trustManagers : ${trustManagerFactory.trustManagers.joinToString()}")
        }

        trustManager = trustManagerFactory.trustManagers[0] as X509TrustManager

        return TLSSocketFactory(keyManagerFactory.keyManagers, trustManagerFactory.trustManagers) to trustManager
    }
}