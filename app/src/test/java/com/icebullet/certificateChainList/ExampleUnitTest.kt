package com.icebullet.certificateChainList

import androidx.test.platform.app.InstrumentationRegistry
import com.icebullet.certificateChainList.net.NetRequest
import org.junit.Test


private val SERVER_CERT = R.raw.certificate_server

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        NetRequest.NetRequestImp.callmeCheck(Unit)
    }
}
