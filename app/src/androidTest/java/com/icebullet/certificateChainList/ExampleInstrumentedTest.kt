package com.icebullet.certificateChainList

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.icebullet.certificateChainList.net.AppActivityConnector
import com.icebullet.certificateChainList.net.HttpCryptoUtils
import com.icebullet.certificateChainList.net.NetRequest

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {

    private val SERVER_CERT = R.raw.certificate_server

    val certificateServer: X509Certificate by lazy {
        InstrumentationRegistry.getInstrumentation().targetContext.resources.openRawResource(SERVER_CERT).use {
            CertificateFactory.getInstance("X.509").generateCertificate(it) as X509Certificate
        }
    }

    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().context
        NetRequest.NetRequestImp.callmeCheck(Unit)

        assertEquals("com.icebullet.androidkeyattestation", appContext.packageName)
    }
}
